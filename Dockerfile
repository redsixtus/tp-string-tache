

FROM maven:3-jdk-8-slim
COPY . /sourcestp-helloworld
RUN cd /sourcestp-helloworld && mvn clean package
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/sourcestp-helloworld/target/tp-helloworld-0.0.1-SNAPSHOT.jar"]
