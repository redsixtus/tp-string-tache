package com.example.tphelloworld;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller

public class TacheController {

  private List<Tache> tacheList = new ArrayList<>();

  @GetMapping("/taches")
  public String getTaches(Model model) {
    Tache tache1 = new Tache("Nouvelle tache", 1, false);
    Tache tache2 = new Tache("Une deuxième tâche", 1, false);
    Tache tache3 = new Tache("Une troisième tâche", 2, true);
    Tache tache4 = new Tache("Une quatrième tâche", 2, false);

    tacheList.add(tache1);
    tacheList.add(tache2);
    tacheList.add(tache3);



    model.addAttribute("taches", tacheList);


    return "liste-taches";
  }
  @GetMapping("/ajout")
  public String formulaire(Model model) { // M de MVC
    Tache tache = new Tache(); // article vide
    model.addAttribute("tache", tache);
    return "tchat"; // nom de la vue (V de MVC)
  }


  @PostMapping("/ajout")

  public String formulaire(@Valid @ModelAttribute("article") Tache tache, Errors errors) { // M de MVC
    if (errors.hasErrors()) {
// on renvoie la page HTML du formulaire
// pas besoin de remplir le model, il a déjà un article, les champs seront pré-remplis
      return "formulaire";
    } else {
      tacheList.add(tache);
// cas sans erreur
// on enregistre l'article en base de données ou ailleurs et on redirige l'utilisateur sur une autre page
      return "redirect:/taches";
    }
  }
}